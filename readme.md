# Виджет

Подключить скрипт в нужном месте на страннице

```html
<div id="messages"></div>
<div id="counter" class="counter"></div>
```

Подключить скрипт внизу страницы

```html
<script src="./script.js"></script>
<script>
  Widget("counter", "messages");
</script>
```

`counter` для счётчика сообщений,
`messages` для списка сообщений
