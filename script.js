let styleWidget = `
.list-messages {
  list-style: none;
  padding: 0;
  margin: 0;
  width: 100%;
}

.list-messages-item {
  display: flex;
  flex-direction: column;
  margin-bottom: 15px;
}

.massage-title {
  color: #4e95ff;
  margin: 10px 0;
}

.massage-author {
  color: #7e7e7e;
  text-decoration: none;
}
.massage-link {
  text-decoration: none;
}

.massage-status {
  color: #7e7e7e;
  font-weight: 300;
}

.active {
  font-weight: 700;
  color: #000000;
}

.list-messages-item:hover {
  cursor: pointer;
}

.btn {
background-color: #4e95ff;
  border: none;
  padding: 10px 15px;
  color: #ffffff;
  border-radius: 5px;
  box-shadow: 1px 1px 0 #fff, inset 0 1px 1px rgb(0 0 0 / 30%);
}

.btn:hover {
  background-color: #a7bdfa;
  border: none;
}

.btn:active {
  background-color: #ffffff;
  color: #000000;
  border: none;
}`;

const messages = [
  {
    id: 1,
    title: "Глобальное потепление в Антарктике.",
    author: "Мария",
    dataTime: "12.03.2020",
    link: "Подробнее",
    status: false,
  },
  {
    id: 2,
    title: "Геймеры за виртуальный мир.",
    author: "Павел",
    dataTime: "1.10.2019",
    link: "Подробнее",
    status: false,
  },
  {
    id: 3,
    title: "Миру мир!",
    author: "Любовь",
    dataTime: "29.07.2008",
    link: "Подробнее",
    status: false,
  },
  {
    id: 4,
    title: "Облака снова затянули небо над Санкт-Петербургом.",
    author: "Александра",
    dataTime: "10.05.2003",
    link: "Подробнее",
    status: false,
  },
  {
    id: 5,
    title: "Как войти в it?",
    author: "Роман",
    dataTime: "12.03.2008",
    link: "Подробнее",
    status: false,
  },
  {
    id: 6,
    title: "Надо больше книжек читать",
    author: "Евгений",
    dataTime: "12.03.2020",
    link: "Подробнее",
    status: true,
  },
  {
    id: 7,
    title: "Спорт наше все!",
    author: "Генадий",
    dataTime: "5.12.2005",
    link: "Подробнее",
    status: true,
  },
  {
    id: 8,
    title: "Чистый код должен быть чистым",
    author: "Соня",
    dataTime: "20.01.2020",
    link: "Подробнее",
    status: true,
  },
  {
    id: 9,
    title: "Мы должны улучшать медицину!",
    author: "Ринат",
    dataTime: "20.06.2001",
    link: "Подробнее",
    status: false,
  },
];

let listMessages = messages.map((v, num) => {
  let _li = document.createElement("li");
  _li.className = "list-messages-item";
  _li.innerHTML = `<h2 class="massage-title">${v.title}</h2><a href="#" class="massage-author">Автор:${v.author}</a><a href="#" class="massage-link">${v.link}</a><time class="massage-time">${v.dataTime}</time>`;
  let _status = document.createElement("status");
  _status.className = `massage-status ${v.status ? "active" : ""}`;
  _status.innerHTML = v.status ? "прочитано" : "не прочитано";
  _li.appendChild(_status);
  _li.addEventListener("click", () => {
    _status.innerHTML = "прочитано";
    _status.classList.add("active");
  });
  return _li;
});

function Widget(counterRef, listMessagesRef) {
  let counterEl = document.getElementById(counterRef);
  let listMessagesEl = document.getElementById(listMessagesRef);
  let styleEl = document.createElement("style");
  styleEl.innerHTML = styleWidget;
  document.body.appendChild(styleEl);
  let btn = document.createElement("button");
  btn.innerHTML = `<span>${messages.length}</span> сообщений`;
  btn.className = "btn";
  btn.type = "button";
  let _ul = document.createElement("ul");
  _ul.className = "list-messages";
  listMessages.map((v) => {
    _ul.appendChild(v);
  });
  listMessagesEl.appendChild(_ul);
  listMessagesEl.style.display = "none";

  btn.addEventListener("click", function () {
    if (listMessagesEl.style.display == "none") {
      listMessagesEl.style.display = "flex";
    } else {
      listMessagesEl.style.display = "none";
    }
  });

  counterEl.appendChild(btn);
}
